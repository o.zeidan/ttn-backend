package dashboard

import "time"

// TTNBody is the structure of a TTN HTTP message
type TTNBody struct {
	AppID          string                 `json:"app_id"`
	DevID          string                 `json:"dev_id"`
	HardwareSerial string                 `json:"hardware_serial"`
	Port           int                    `json:"port"`
	Counter        int                    `json:"counter"`
	PayloadRaw     string                 `json:"payload_raw"`
	PayloadFields  map[string]interface{} `json:"payload_fields"`
	Metadata       struct {
		Time time.Time `json:"time"`
	} `json:"metadata"`
	DownlinkURL string `json:"downlink_url"`
}

// TrashcanPayload is the payload of a sensor of trashcan type
type TrashcanPayload struct {
	Battery float64 `json:"battery"`
	Level   float64 `json:"level"`
}

// ParkinglotPayload is the payload of a sensor of parking lot type
type ParkinglotPayload struct {
	Battery  float64 `json:"battery"`
	Occupied bool    `json:"occupied"`
}

// Response contains the important fields of a downlink message from the dashboard
type Response struct {
	Value struct {
		Interval string `json:"interval"`
	} `json:"value"`
	IDSensor int `json:"id_sensor"`
}
