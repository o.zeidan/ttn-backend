package dashboard

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"sync"
)

type payload struct {
	Minutes byte `json:"minutes"`
}

type ttnMessage struct {
	DevID     string  `json:"dev_id"`
	Port      int     `json:"port"`
	Confirmed bool    `json:"confirmed"`
	Payload   payload `json:"payload_fields"`
}

var mutex sync.Mutex

func sendToTTN(minutes byte, deviceID, downlinkURL string) {
	mutex.Lock()
	defer mutex.Unlock()

	msg := &ttnMessage{
		deviceID,
		1,
		true,
		payload{
			minutes,
		},
	}

	jsonString, err := json.Marshal(msg)
	logErr(err)

	fmt.Println("Sending", string(jsonString), "to TTN")
	req, err := http.NewRequest("POST", downlinkURL, bytes.NewBuffer(jsonString))
	logErr(err)

	res, err := http.DefaultClient.Do(req)
	logErr(err)

	bytes, err := ioutil.ReadAll(res.Body)
	logErr(err)

	fmt.Println("Received Status Code:", res.StatusCode, "Content:", string(bytes))
}
