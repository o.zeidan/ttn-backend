package dashboard

import (
	"encoding/json"
	"fmt"
	"net/url"
	"strconv"
	"time"

	"github.com/gorilla/websocket"
)

const apiHost = "kranzj-office.spdns.de:8888"

var dashboardAdress = url.URL{
	Scheme: "ws",
	Host:   apiHost,
	Path:   "/ws",
}

var chans = make(map[int]chan interface{})

type wastebinMessage struct {
	Value     wastebinPayload `json:"value"`
	Timestamp time.Time       `json:"timestamp"`
}

type wastebinPayload struct {
	Level   float64 `json:"level"`
	Battery float64 `json:"battery"`
}

type parkinglotPayload struct {
	Occupied bool    `json:"occupied"`
	Battery  float64 `json:"battery"`
}

func connectToSensor(id int) {
	if _, ok := chans[id]; ok {
		fmt.Println("Already connected to this sensor")
		return
	}

	url := fmt.Sprintf("%s/%d", dashboardAdress.String(), id)
	fmt.Println("Connecting to URL", url)

	c, _, err := websocket.DefaultDialer.Dial(url, nil)
	logErr(err)
	if err != nil {
		return
	}

	dataChan := make(chan interface{})
	chans[id] = dataChan

	go func() {
		for {
			fmt.Println("received websocket message")
			_, message, err := c.ReadMessage()
			logErr(err)

			response := &Response{}

			err = json.Unmarshal(message, response)

			if err != nil {
				fmt.Println("received invalid message")
				continue
			}

			i, err := strconv.Atoi(response.Value.Interval)

			if err != nil {
				fmt.Println("received invalid message")
				continue
			}

			fmt.Println(i, "minutes")

			if dev, err := findByDashboard(id); err == nil {
				sendToTTN(byte(i), dev.DevID, dev.DownlinkURL)
			}
		}
	}()

	go func() {
		for msg := range dataChan {
			fmt.Println("Sending message", msg, "to sensor id", id)
			err := c.WriteJSON(&msg)
			logErr(err)
		}
	}()
}

func sendWastebinData(id int, level, battery float64) {
	sendToSensor(id, wastebinPayload{
		level,
		battery,
	})
}

func sendParkinglotData(id int, occupied bool, battery float64) {
	sendToSensor(id, parkinglotPayload{
		occupied,
		battery,
	})
}

func sendToSensor(id int, payload interface{}) {
	if _, ok := chans[id]; !ok {
		connectToSensor(id)
	}

	channel := chans[id]

	channel <- struct {
		Value     interface{} `json:"value"`
		Timestamp time.Time   `json:"timestamp"`
	}{payload, time.Now()}
}

func logErr(err error) {
	fmt.Println(err)
}
