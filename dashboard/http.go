package dashboard

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"time"

	"github.com/pkg/errors"
)

var dashboardHTTPAdress = url.URL{
	Scheme: "http",
	Host:   apiHost,
}

var client = http.Client{
	Timeout: time.Duration(5 * time.Second),
}

type sensor struct {
	ID           int    `json:"id"`
	Name         string `json:"name"`
	Note         string `json:"note"`
	IDSensorType int    `json:"id_sensor_type"`
}

// CreateSensor creates a sensor at the dashboard and returns its sensorID
func CreateSensor(sensorType sensorType, appID string, devID string) (int, error) {
	payload := &sensor{
		Name:         fmt.Sprintf("LoRa Device %s", devID),
		Note:         fmt.Sprintf("App ID %s", appID),
		IDSensorType: int(sensorType),
	}

	fmt.Println("Creating sensor for devid", devID, "appid", appID)

	jsonString, err := json.Marshal(payload)

	if err != nil {
		return -1, wrapErr(err)
	}

	u := url.URL{
		Scheme: "http",
		Host:   apiHost,
		Path:   "api/sensors",
	}

	req, err := http.NewRequest("POST", u.String(), bytes.NewBuffer(jsonString))
	if err != nil {
		return -1, wrapErr(err)
	}

	req.Header.Add("Content-Type", "application/json")

	res, err := client.Do(req)
	if err != nil {
		return -1, wrapErr(err)
	}

	defer res.Body.Close()
	err = json.NewDecoder(res.Body).Decode(payload)
	if err != nil {
		return -1, wrapErr(err)
	}

	return payload.ID, nil
}

func wrapErr(err error) error {
	return errors.Wrap(err, "couldn't create sensor")
}
