package dashboard

import (
	"fmt"
	"log"

	"github.com/pkg/errors"
	"gitlab.com/o.zeidan/ttn-backend/persistence"
)

type sensorType int

const (
	trashcan   sensorType = 454
	parkinglot sensorType = 455
)

type device struct {
	AppID       string
	DevID       string
	DownlinkURL string
	DashboardID int
	SensorType  sensorType
}

const devicesFileName = "devices"

var (
	errNotFound   = errors.New("Device not registered")
	errInvalidApp = errors.New("Sending from invalid TTN app")
)

var (
	persistor  persistence.JSONPersistor
	deviceList = make([]device, 0)
)

// Initialize initializess the device management
func Initialize() error {
	err := createPersistor()
	if err != nil {
		return err
	}

	err = loadDevices()
	if err != nil {
		return err
	}

	connectToSensors()

	return nil
}

func createPersistor() error {
	var err error
	persistor, err = persistence.New(devicesFileName, false)

	if err != nil {
		return errors.Wrap(err, "couldn't create persistor")
	}

	return nil
}

func loadDevices() error {
	err := persistor.LoadData(&deviceList)
	if err != nil {
		return errors.Wrap(err, "couldn't load devices from file")
	}

	return nil
}

func connectToSensors() {
	for _, dev := range deviceList {
		connectToSensor(dev.DashboardID)
	}
}

// RegisterMessage registeres the device if necessary and initiates the mapping and message redirections
func RegisterMessage(body TTNBody) error {
	dev, err := findByTTN(body.AppID, body.DevID)
	log.Println("Registering message, app id", body.AppID, "dev id", body.DevID)

	// TODO: figure out way to determine sensortype
	var sensorType sensorType

	if body.AppID == "trashcan" {
		sensorType = trashcan
	} else if body.AppID == "parkinglot2" {
		sensorType = parkinglot
	} else {
		return errInvalidApp
	}

	if err != nil {
		sensorID, err := CreateSensor(sensorType, body.AppID, body.DevID)

		if err != nil {
			return err
		}

		fmt.Println("created sensor with id", sensorID)

		dev = device{
			body.AppID,
			body.DevID,
			body.DownlinkURL,
			sensorID,
			sensorType,
		}

		deviceList = append(deviceList, dev)
		persistor.SaveData(&deviceList)
	}

	if dev.SensorType == trashcan {
		sendWastebinData(dev.DashboardID,
			body.PayloadFields["level"].(float64),
			body.PayloadFields["battery"].(float64))
	} else {
		sendParkinglotData(dev.DashboardID,
			!body.PayloadFields["occupied"].(bool),
			body.PayloadFields["battery"].(float64))
	}

	return nil
}

func findByTTN(appID string, devID string) (device, error) {
	for _, device := range deviceList {
		if device.AppID == appID && device.DevID == devID {
			return device, nil
		}
	}

	return device{}, errNotFound
}

func findByDashboard(ID int) (device, error) {
	for _, device := range deviceList {
		if device.DashboardID == ID {
			return device, nil
		}
	}

	return device{}, errNotFound
}

// Close closes the persistor
func Close() {
	persistor.Close()
}
