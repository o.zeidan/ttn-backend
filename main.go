package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"html/template"
	"io"
	"net/http"
	"time"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"gitlab.com/o.zeidan/ttn-backend/dashboard"
	"gitlab.com/o.zeidan/ttn-backend/persistence"
)

type ttnMini struct {
	DeviceID   string
	AppID      string
	PayloadHex string
	TimeString string
}

const saveFileName = "ttn_saves"
const ttnBackendPort = 8081

var (
	receivedPackages = make([]ttnMini, 0)
	persistor        persistence.JSONPersistor
)

func main() {
	var err error
	persistor, err = persistence.New(saveFileName, true)

	if err != nil {
		panic(err)
	}

	defer persistor.Close()

	err = persistor.LoadData(&receivedPackages)

	if err != nil {
		fmt.Println(err)
	}

	err = dashboard.Initialize()
	defer dashboard.Close()

	if err != nil {
		fmt.Println(err)
	}

	e := echo.New()
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	e.GET("/", index)
	e.POST("/clear", clearData)
	e.POST("/data", parseData)
	e.Static("/static", "assets")

	t := &ttnTemplate{
		templates: template.Must(template.ParseGlob("index.html")),
	}

	e.Renderer = t

	e.Logger.Fatal(e.Start(fmt.Sprintf(":%d", ttnBackendPort)))
}

func parseData(c echo.Context) error {
	var ttnPackage dashboard.TTNBody

	err := json.NewDecoder(c.Request().Body).Decode(&ttnPackage)
	if err != nil {
		return errors.New("Error while parsing response: " + err.Error())
	}
	fmt.Printf("Received payload %x\n", ttnPackage.PayloadRaw)

	err = dashboard.RegisterMessage(ttnPackage)

	if err != nil {
		fmt.Println("couldn't register device:", err)
	}

	loc, err := time.LoadLocation("Europe/Berlin")

	if err != nil {
		panic("oh shit")
	}

	miniPackage := ttnMini{
		ttnPackage.DevID,
		ttnPackage.AppID,
		fmt.Sprintf("%+v", ttnPackage.PayloadFields),
		time.Now().In(loc).Format("2. Jan, 15:04:05"),
	}
	receivedPackages = append(receivedPackages, miniPackage)
	persistor.SaveData(receivedPackages)
	return nil
}

func clearData(c echo.Context) error {
	receivedPackages = make([]ttnMini, 0)
	persistor.SaveData(receivedPackages)
	return c.Redirect(http.StatusFound, "/")
}

type ttnTemplate struct {
	templates *template.Template
}

func (t *ttnTemplate) Render(w io.Writer, name string, data interface{}, c echo.Context) error {
	return t.templates.ExecuteTemplate(w, name, data)
}

func index(c echo.Context) error {
	return c.Render(http.StatusOK, "index", receivedPackages)
}
