package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
)

var client = http.DefaultClient

func sendToThingsBoard(level, battery float64) {
	jsonString, err := json.Marshal(struct {
		Level   float64 `json:"level"`
		Battery float64 `json:"battery"`
	}{level, battery})
	logErr(err)

	fmt.Println("sending", string(jsonString), "to thingsboard")

	req, err := http.NewRequest(
		"POST",
		"https://git.traphouse.pw/api/v1/U3sOf5xC3obWlhInKgr6/telemetry",
		bytes.NewBuffer(jsonString),
	)
	logErr(err)

	resp, err := client.Do(req)
	logErr(err)

	fmt.Println("Response Code:", resp.StatusCode)
}

func logErr(err error) {
	if err != nil {
		fmt.Println(err.Error())
	}
}
