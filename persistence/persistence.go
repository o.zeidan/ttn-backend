package persistence

import (
	"encoding/json"
	"fmt"
	"os"

	"github.com/pkg/errors"
)

// JSONPersistor is a struct that lets you serialize structs into the json format on disk
type JSONPersistor struct {
	file *os.File
}

// New creates a new JSONPersistor
// File will be read/written into a file named filename.json
// When logging is set to true, the file will be appended on saving
func New(fileName string, logging bool) (JSONPersistor, error) {
	flags := os.O_CREATE | os.O_RDWR
	if logging {
		flags |= os.O_APPEND
	}
	newFile, err := os.OpenFile(fmt.Sprintf("%s.json", fileName), flags, 0600)

	if err != nil {
		return JSONPersistor{}, errors.Wrap(err, "cannot open file")
	}

	return JSONPersistor{newFile}, nil
}

// SaveData marshals the struct data into a the file
func (p JSONPersistor) SaveData(data interface{}) error {
	p.file.Truncate(0)
	p.file.Seek(0, 0)

	enc := json.NewEncoder(p.file)
	enc.SetIndent("", "\t")

	err := enc.Encode(data)

	if err != nil {
		return errors.Wrap(err, "cant encode data")
	}

	return nil
}

// LoadData loads the data on disk into the struct data
func (p JSONPersistor) LoadData(data interface{}) error {
	err := json.NewDecoder(p.file).Decode(data)

	if err != nil {
		return errors.Wrap(err, "couldnt decode json from file")
	}

	return nil
}

// Close flushes and closes the file
func (p JSONPersistor) Close() {
	p.file.Close()
}
